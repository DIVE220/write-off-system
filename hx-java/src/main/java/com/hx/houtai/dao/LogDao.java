package com.hx.houtai.dao;

import com.hx.houtai.pojo.Log;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Mr.Fang
 * @description 日志dao层
 * @date 2021/7/30
 */

@Component
@Mapper
public interface LogDao {


    /*
     *插入日志
     * @author 方磊
     * @date 2021/7/30
      * @param log
     * @return void
     */
    public void insertLog(Log log);


    /*
     *查询日志记录
     * @author 方磊
     * @date 2021/8/3
      * @param
     * @return java.util.List<com.hx.houtai.pojo.Log>
     */
    public List<Log> selLogInfo(@Param("logTime") String logTime ,@Param("logType") String logType ,@Param("logUser") String logUser);




}
