package com.hx.houtai.dao;

import com.hx.houtai.pojo.*;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author Mr.Fang
 * @description 员工dao层
 * @date 2021/8/5
 */

@Mapper
@Component
public interface EmployeeDao {


    /*
     *查询部门信息
     * @author 方磊
     * @date 2021/8/11
      * @param
     * @return java.util.List<com.hx.houtai.pojo.Department>
     */
    public List<Department> selEmDepartmentInfo();


    /*
     *查询公司员工
     * @author 方磊
     * @date 2021/8/5
      * @param departmentID
     * @param keyWord
     * @return java.util.List<com.hx.houtai.pojo.Employee>
     */
    public List<Employee> selEmployeeInfo( @Param("deptNum") String deptNum , @Param("keyWord") String keyWord , @Param("employeeStatus")String employeeStatus , @Param("employeeWorkNature")String employeeWorkNature);


    /*
     *查询每个工作状态的人数
     * @author 方磊
     * @date 2021/8/16
     * @param
     * @return com.hx.houtai.pojo.WorkStatus
     */
    public WorkStatus selworkStatusNum();


    /*
     *获取部门职位树
     * @author 方磊
     * @date 2021/8/25
      * @param
     * @return java.util.List<com.hx.houtai.pojo.Department>
     */
    public List<Department> seldePostTree();


    /*
     *更改员工简单信息 
     * @author 方磊
     * @date 2021/10/12
      * @param employee
     * @return int
     */
    public int updateEmployeeInfo(Employee employee);


    /*
     *查询院校
     * @author 方磊
     * @date 2021/10/12
     * @param keyWords
     * @return java.util.List<com.hx.houtai.pojo.School>
     */
    public List<School> selSchool(@Param("keyWords")String keyWords);


    /*
     * 新增员工
     * @Author 方磊
     * @Date 2022/3/4
     * @Param
     * @return
     */
    public int addEmployee(Employee employee);


    /**
      * 查询职位的编码和人数
      * @Author 方磊
      * @Date 2022/3/31
      * @Param [employeePosition]
      * @return java.util.List<java.util.Map<java.lang.String,java.lang.String>>
      */
    public List<Map<String , String>> getJobNumber(@Param("employeePosition") String employeePosition);

}
