package com.hx.houtai.dao;

import com.hx.houtai.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * @author Mr.Fang
 * @Title: userDao
 * @Package hx
 * @Description: userDao层
 * @date 2021/3/5 9:52
 */

@Component
@Mapper
public interface UserDao {
    public User logon(User user);
}