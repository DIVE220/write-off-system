package com.hx.houtai.util;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Mr.Fang
 * @Title: JwtUtil
 * @Package hx
 * @Description: jwt工具类
 * @date 2021/3/5 9:52
 */


public class JwtUtil {
    static final String SECRET = "ALei";


    //生成token
    public static String generateToken(String name) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("name", name);
        String jwt = Jwts.builder().setClaims(map)
                .setAudience("FangLei")
                .setExpiration(new Date(System.currentTimeMillis() + (86400000L)))
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();
        return jwt;
    }

    //token解析
    public static void validateToken(String token) {
        try {
            Map<String, Object> body = Jwts.parser()
                    .setSigningKey(SECRET)
                    .parseClaimsJws(token)
                    .getBody();
        }catch (Exception e){
            throw new IllegalStateException("Token Error(token验证失败):" + e.getMessage());
        }

    }
}
