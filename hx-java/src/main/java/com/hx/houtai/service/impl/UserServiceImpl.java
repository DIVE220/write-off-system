package com.hx.houtai.service.impl;

import com.hx.houtai.config.Log;
import com.hx.houtai.dao.UserDao;
import com.hx.houtai.pojo.Result;
import com.hx.houtai.pojo.User;
import com.hx.houtai.service.UserService;
import com.hx.houtai.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.alibaba.druid.util.Utils.md5;

/**
 * @author Mr.Fang
 * @Title: UserServiceImpl
 * @Package hx
 * @Description: userServiceImpl层
 * @date 2021/3/4 21:02
 */

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;


    /*
     *登录
     * @author 方磊
     * @date 2021/5/26
     * @param user
     * @return com.hx.houtai.pojo.Result
     */
    @Override
    public Result logon(User user) {
        Result result = new Result();
        User newUser = this.userDao.logon(user);
        if (newUser != null) {
            result.setCode("200");
            result.setMsg("查询成功");
            result.setData(newUser);
            result.setToken(JwtUtil.generateToken(user.getGuid()));
        } else {
            result.setCode("400");
            result.setMsg("密码或用户名有误");
        }
        return result;

    }

    public static void main(String[] args) {

    }
}
