package com.hx.houtai.service;

import com.hx.houtai.pojo.Result;
import com.hx.houtai.pojo.User;

/**
 * @author Mr.Fang
 * @Title: UserService
 * @Package hx
 * @Description: userService层
 * @date 2021/3/4 21:00
 */

public interface UserService {

    Result logon(User user);
}
