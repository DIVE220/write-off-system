package com.hx.houtai.service.impl;

import com.hx.houtai.config.Log;
import com.hx.houtai.dao.EmployeeDao;
import com.hx.houtai.pojo.*;
import com.hx.houtai.service.EmployeeService;
import io.swagger.models.auth.In;
import lombok.Data;
import org.apache.poi.util.ArrayUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;
import schemasMicrosoftComOfficeOffice.STInsetMode;

import java.beans.Transient;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Mr.Fang
 * @description 员工impl
 * @date 2021/8/5
 */

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeDao employeeDao;


    /**
     *查询部门信息
     * @author 方磊
     * @date 2021/8/11
     * @param
     * @return com.hx.houtai.pojo.Result
     */
    @Override
    public Result selEmDepartmentInfo() {

        Result result = new Result();
        try {
            List<Department> list = this.employeeDao.selEmDepartmentInfo();
            if (!list.isEmpty()) {
                Department department = new Department();
                department.setDeptNum("");
                department.setDeptName("全部");
                list.add(0, department);
                result.setCode("200");
                result.setMsg("查询成功");
                result.setData(list);
            } else {
                result.setCode("400");
                result.setMsg("暂无数据");
            }
        } catch (NullPointerException e) {
            e.fillInStackTrace();
            result.setCode("500");
            result.setMsg("程序出错，请联系管理员");
        }
        return result;
    }


    /**
     *查询员工信息
     * @author 方磊
     * @date 2021/8/16
     * @param deptNum
     * @param keyWord
     * @param employeeStatus
     * @param employeeWorkNature
     * @return com.hx.houtai.pojo.Result
     */
    @Override
    public Result selEmployeeInfo(String deptNum, String keyWord, String employeeStatus, String employeeWorkNature) {
        Result result = new Result();
        try {
            List<Employee> list = this.employeeDao.selEmployeeInfo(deptNum, keyWord, employeeStatus, employeeWorkNature);
            if (!list.isEmpty()) {
                result.setCode("200");
                result.setMsg("查询成功");
                result.setData(list);
            } else {
                result.setCode("400");
                result.setMsg("暂无数据");
            }
        } catch (NullPointerException e) {
            e.fillInStackTrace();
            result.setCode("500");
            result.setMsg("程序出错，请联系管理员");
        }
        return result;
    }


    /**
     *查询不同状态的人数
     * @author 方磊
     * @date 2021/8/16
     * @param
     * @return com.hx.houtai.pojo.Result
     */
    @Override
    public Result selworkStatusNum() {
        Result result = new Result();
        try {
            WorkStatus workStatus = this.employeeDao.selworkStatusNum();
            if (workStatus != null) {
                result.setCode("200");
                result.setMsg("查询成功");
                result.setData(workStatus);
            } else {
                result.setCode("400");
                result.setMsg("暂无统计人数");
            }
        } catch (NullPointerException e) {
            e.fillInStackTrace();
            result.setCode("500");
            result.setMsg("程序出错，请联系管理员");
        }
        return result;
    }


    /**
     *获取部门职位树
     * @author 方磊
     * @date 2021/8/25
     * @param
     * @return com.hx.houtai.pojo.Result
     */
    @Override
    public Result seldePostTree() {
        Result result = new Result();
        List<Department> depList = this.employeeDao.seldePostTree();
        System.out.println(depList);
        result.setData(depList);
        return result;
    }


    /**
     *更改员工简单信息
     * @author 方磊
     * @date 2021/9/28
     * @param ruleForm
     * @return com.hx.houtai.pojo.Result
     */
    @Override
    public Result updateEmployeeInfo(Employee ruleForm) {
        Result result = new Result();
        int num = this.employeeDao.updateEmployeeInfo(ruleForm);
        if (num >= 1) {
            result.setCode("200");
            result.setMsg("员工信息修改完毕");
        } else {
            result.setCode("200");
            result.setMsg("员工信息修改失败，请联系管理员");
        }
        return result;
    }


    /**
     *查询院校
     * @author 方磊
     * @date 2021/3/3
     * @param keyWords
     * @return com.hx.houtai.pojo.Result
     */
    @Override
    public Result selSchool(String keyWords) {
        Result result = new Result();
        List<School> list = this.employeeDao.selSchool(keyWords);
        if (list != null && !list.isEmpty()) {
            result.setCode("200");
            result.setMsg("查询成功");
            result.setData(list);
        } else {
            result.setCode("400");
            result.setMsg("查询成功");
        }
        return result;
    }


    /**
     *新增员工
     * @Author 方磊
     * @Date 2022/3/4
     * @Param [employee]
     * @return com.hx.houtai.pojo.Result
     */
    @Override
    public Result addEmployee(Employee employee, String employeeProbationUnit) {
        Result result = new Result();
        //UUID
        employee.setEmployeeID(UUID.randomUUID().toString());
        //从身份证号码获取生日
        employee.setEmployeeBirth(this.getBirthday(employee.getEmployeeIDNum()));
        //工号
        employee.setEmployeeJobNumber(this.getJobNumber(employee.getEmployeePosition()));
        //计算试用期
        if (employeeProbationUnit.equals("个月")) {
            employee.setEmployeeProbation(this.getDays(employee.getEmployeeEntryDate() , employee.getEmployeeProbation()));
        }
        //新增员工
        int num = this.employeeDao.addEmployee(employee);
        if(num <= 0){
            result.setCode("400");
            result.setMsg("新增失败,请联系管理员");
        }else{
            result.setCode("200");
            result.setMsg("新增成功");
        }
        return result;
    }


    /**
     * 从身份证号中提取出生年月日
     * @Author 方磊
     * @Date 2022/3/4
     * @Param [birth]
     * @return java.lang.String
     */
    public String getBirthday(String birth) {
        StringBuilder str = new StringBuilder(birth.substring(6, 14));
        str.insert(4, ".");
        str.insert(7, ".");
        return str.toString();
    }


    /**
     * 计算试用期的天数
     * @Author 方磊
     * @Date 2022/3/4
     * @Param [days]
     * @return int
     */
    public int getDays(String startDay , int days) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        Date date = null;
        try {
            date = format.parse(startDay)      ;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //获取几个月后的日期
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, days);
        //获取入职开始日期
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date);
        //计算相差的天数
        int i = 0;
        while (cal2.getTime().before(cal.getTime())) {
            cal2.add(Calendar.DATE, 1);
            i++;
        }
        return i+1;
    }



    /**
      * 获取员工工号
      * @Author 方磊
      * @Date 2022/3/31
      * @Param [employeePosition]
      * @return java.lang.String
      */
    public String getJobNumber(String employeePosition)   {
        StringBuilder str = new StringBuilder();
        //查询职位的编码和人数
        List<Map<String , String>> list = this.employeeDao.getJobNumber(employeePosition);
        System.out.println(list);
        Map<String , String> map = list.get(0);
        //获取职位唯一标识编码
        String first = map.get("positionCoding");
        str.append(first);
        int two = Integer.parseInt(String.valueOf(map.get("count")));
        two++;
        //补位加0
        if(two < 10 && two > 0){
            str.append("00");
            str.append(two);
        }else if(two >= 10 && two < 100){
            str.append("0");
            str.append(two);
        }else if(two >= 100){      
            str.append(two);
        }
        return str.toString();
    }


}
