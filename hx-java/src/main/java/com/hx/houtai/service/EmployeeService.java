package com.hx.houtai.service;

import com.hx.houtai.pojo.Employee;
import com.hx.houtai.pojo.Result;

/**
 * @author Mr.Fang
 * @description 员工service层
 * @date 2021/8/5
 */
public interface EmployeeService {


    /*
     *查询部门信息
     * @author 方磊f
     * @date 2021/8/11
      * @param
     * @return com.hx.houtai.pojo.Result
     */
    public Result selEmDepartmentInfo();


    /*
     *查询员工信息
     * @author 方磊
     * @date 2021/8/16
      * @param deptNum
     * @param keyWord
     * @param employeeStatus
     * @param employeeWorkNature
     * @return com.hx.houtai.pojo.Result
     */
    public Result selEmployeeInfo(String deptNum , String keyWord , String employeeStatus , String employeeWorkNature);


    /*
     *查询不同状态的人数
     * @author 方磊
     * @date 2021/8/16
      * @param
     * @return com.hx.houtai.pojo.Result
     */
    public Result selworkStatusNum();


    /*
     *获取部门职位树
     * @author 方磊
     * @date 2021/8/25
      * @param
     * @return com.hx.houtai.pojo.Result
     */
    public Result seldePostTree();


    /*
     *更改员工简单信息
     * @author 方磊
     * @date 2021/9/28
      * @param ruleForm
     * @return com.hx.houtai.pojo.Result
     */
    public Result updateEmployeeInfo(Employee ruleForm);

    /*
     *查询院校
     * @author 方磊
     * @date 2022/3/3
     * @param keyWorks
     * @return com.hx.houtai.pojo.Result
     */
    public Result selSchool(String keyWords);

    
    /* *
     * @Author 方磊
     * @Date 2022/3/4
     * @Param [employee]
     * @return com.hx.houtai.pojo.Result
     */
    public Result addEmployee(Employee employee , String employeeProbationUnit);
}
