package com.hx.houtai.service.impl;

import com.hx.houtai.dao.LogDao;
import com.hx.houtai.pojo.Log;
import com.hx.houtai.pojo.Result;
import com.hx.houtai.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Mr.Fang
 * @description 日志impl层
 * @date 2021/8/3
 */

@Service
public class LogServiceImpl implements LogService {

    @Autowired
    private LogDao logDao;


    /*
     *查询日志列表
     * @author 方磊
     * @date 2021/8/4
      * @param logTime
     * @param logType
     * @param logUser
     * @return com.hx.houtai.pojo.Result
     */
    @Override
    public Result selLogInfo(String logTime , String logType , String logUser) {
        Result result = new Result();
        List<Log> logList = this.logDao.selLogInfo(logTime, logType, logUser);
        if(logList.isEmpty()){
            result.setCode("400");
            result.setMsg("查询列表为空!");
        }else{
            result.setCode("200");
            result.setMsg("查询成功");
            result.setData(logList);
        }
        return result;
    }
}
