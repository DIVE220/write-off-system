package com.hx.houtai.service;

import com.hx.houtai.pojo.Result;

/**
 * @author Mr.Fang
 * @description 日志service层
 * @date 2021/8/3
 */
public interface LogService {


    /*
     *查询日志列表
     * @author 方磊
     * @date 2021/8/4
      * @param logTime
     * @param logType
     * @param logUser
     * @return com.hx.houtai.pojo.Result
     */
    public Result selLogInfo(String logTime , String logType , String logUser);
}
