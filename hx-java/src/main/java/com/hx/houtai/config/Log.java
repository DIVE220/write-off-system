package com.hx.houtai.config;

import java.lang.annotation.*;

/**
 * @author Mr.Fang
 * @description 自定义注解
 * @date 2021/8/2
 */


@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD,ElementType.PARAMETER})
public @interface Log {
    String value() default "";
}
