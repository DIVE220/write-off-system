package com.hx.houtai.controller;

import com.alibaba.fastjson.JSON;
import com.hx.houtai.service.LogService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Mr.Fang
 * @description 日志controller层
 * @date 2021/8/3
 */

@CrossOrigin
@RestController
@RequestMapping("/FL/")
public class LogController {

    @Autowired
    private LogService logService;


    @ApiOperation(value = "获取日志列表")
    @PostMapping("selLogInfo")
    public String selLogInfo(String logTime , String logType , String logUser){
        System.out.println(logTime+logType+logUser);
        String str = JSON.toJSONString(this.logService.selLogInfo(logTime, logType, logUser));
        return str;
    }

}
