package com.hx.houtai.controller;

import com.alibaba.fastjson.JSON;
import com.hx.houtai.config.Log;
import com.hx.houtai.pojo.User;
import com.hx.houtai.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Mr.Fang
 * @Title: User
 * @Package hx
 * @Description: 用户登录
 * @date 2021/3/4 20:59
 */

@RestController
@Api(tags = {"登录页面"})
public class UserController {
    @Autowired
    private UserService userService;


    @ApiOperation(value = "登录")
    @PostMapping("/login")
    public String login(String userName,String userPassword){
        User user = new User();
        user.setUserName(userName);
        user.setUserPassword(userPassword);
        return JSON.toJSON(this.userService.logon(user)).toString();
    }
}
