package com.hx.houtai.controller;

import com.alibaba.fastjson.JSON;
import com.hx.houtai.config.Log;
import com.hx.houtai.pojo.Employee;
import com.hx.houtai.service.EmployeeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author Mr.Fang
 * @description 员工controller层
 * @date 2021/8/5
 */

@RestController
@RequestMapping("/FL/")
@CrossOrigin
@Api(tags = {"员工管理"})
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;


    @ApiOperation(value = "查询部门信息")
    @PostMapping("selEmDepartmentInfo")
    public String selEmDepartmentInfo(){
        return JSON.toJSON(this.employeeService.selEmDepartmentInfo()).toString();
    }


    @ApiOperation(value = "查询员工信息")
    @PostMapping("selEmployeeInfo")
    public String selEmployeeInfo(String deptNum , String keyWord , String employeeStatus){
        String employeeWorkNature = "";
        if("全职".equals(employeeStatus)){
            employeeWorkNature = employeeStatus;
            employeeStatus = "";
        }
        return JSON.toJSON(this.employeeService.selEmployeeInfo(deptNum , keyWord , employeeStatus , employeeWorkNature)).toString();
    }


    @ApiOperation(value = "查询不同状态的人数")
    @PostMapping("selworkStatusNum")
    public String selworkStatusNum(){
        return JSON.toJSON(this.employeeService.selworkStatusNum()).toString();
    }


    @ApiOperation(value = "获取部门职位树")
    @PostMapping("seldePostTree")
    public String seldePostTree(){
        return JSON.toJSON(this.employeeService.seldePostTree()).toString();
    }


    @Log
    @ApiOperation(value = "更改员工简单信息")
    @PostMapping("updateEmployeeInfo")
    public String updateEmployeeInfo( String userName, String ruleForm){
        //解析json
        JSONObject jsonObject = JSONObject.fromObject(ruleForm);
        //转换为对象
        Employee employee = JSON.parseObject(ruleForm , Employee.class);
        return JSON.toJSON(this.employeeService.updateEmployeeInfo(employee)).toString();
    }


    @ApiOperation(value = "查询院校")
    @PostMapping("selSchool")
    public String selSchool(String keyWords){
        return JSON.toJSON(this.employeeService.selSchool(keyWords)).toString();
    }


    @Log
    @ApiOperation(value = "新增员工")
    @PostMapping("addEmployee")
    public String addEmployee(String userName , String employee , String employeeProbationUnit){
        Employee employee1 = JSON.parseObject(employee , Employee.class);
        return JSON.toJSON(this.employeeService.addEmployee(employee1, employeeProbationUnit)).toString();
    }
}
