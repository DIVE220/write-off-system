package com.hx.houtai.pojo;

import lombok.Data;

/**
 * @author Mr.Fang
 * @description 职位实体类
 * @date 2021/8/25
 */

@Data
public class Position {

    //职位id
    private String positionID;
    //职位编号
    private String positionNum;
    //职位名称
    private String positionName;
    //职位所属部门
    private String deptID;
    //职位所属权限
    private String authorityID;
    //创建日期
    private String createDate;
    //修改日期
    private String updateDate;
}
