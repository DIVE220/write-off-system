package com.hx.houtai.pojo;

import lombok.Data;

/**
 * @author Mr.Fang
 * @Title: Result
 * @Package hx
 * @Description: 返回类
 * @date 2021/3/4 21:05
 */

@Data
public class Result {
    private String code;
    private String msg;
    private Object data;
    private String token;
}
