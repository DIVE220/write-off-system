package com.hx.houtai.pojo;

import lombok.Data;

/**
 * @author 方磊
 * @description 用户实体类
 * @date 2020/11/9
 */

@Data
public class User {
    String guid;      //唯一标识
    String userName;  //用户名称
    String userPassword;   //用户密码
}
