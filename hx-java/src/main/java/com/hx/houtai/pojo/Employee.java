package com.hx.houtai.pojo;

import lombok.Data;

/**
 * @author Mr.Fang
 * @description 员工基本信息实体类
 * @date 2021/8/4
 */

@Data
public class Employee {
    //员工id
    private String employeeID;
    //员工工号
    private String employeeJobNumber;
    //员工姓名
    private String employeeName;
    //员工性别
    private String employeeGender;
    //员工出生日期
    private String employeeBirth;
    //员工身份证号
    private String employeeIDNum;
    //员工婚姻状况
    private String employeeMaritalStatus;
    //员工民族
    private String employeeEthnic;
    //员工政治面貌
    private String employeePoliticalStatus;
    //员工电子邮箱
    private String employeeEmail;
    //员工联系电话
    private String employeePhone;
    //员工联系地址
    private String employeeAddress;
    //员工部门
    private String employeeDepartment;
    //部门名称
    private String employeeDepartmentName;
    //员工职位
    private String employeePosition;
    //员工职位名称
    private String employeePositionName;
    //员工基本工资
    private String employeeBasiceWage;
    //员工聘用形式
    private String employeeEmploymentForm;
    //员工最高学历
    private String employeeMaxEducation;
    //员工所属专业
    private String employeeMajor;
    //员工毕业院校
    private String employeeGraduatedSchool;
    //员工入职日期
    private String employeeEntryDate;
    //员工在职状态(0:已离职 1:待入职 2:实习 3:试用 4:正式)
    private String employeeStatus;
    //工作性质(全职，兼职，劳务派遣，外包，退休返聘)
    private String employeeWorkNature;
    //员工试用期时间
    private int employeeProbation;
    //员工合同期限
    private String employeeContractPeriod;
    //创建日期
    private String createDate;
    //修改日期
    private String updateDate;
    //合同公司
    private String employeeContractCompany;

}
