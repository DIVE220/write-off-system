package com.hx.houtai.pojo;

import lombok.Data;

import java.util.List;

/**
 * @author Mr.Fang
 * @description 部门实体类
 * @date 2021/8/11
 */

@Data
public class Department {

    //部门id
    private String deptID;
    //部门编号
    private String deptNum;
    //部门名称
    private String deptName;
    //部门负责人
    private String deptHead;
    //上级部门id
    private String parentDeptID;
    //创建日期
    private String createDate;
    //修改日期
    private String updateDate;
    //所属职位树
    private List<Position> positionList;
}
