package com.hx.houtai.pojo;

import lombok.Data;

/**
 * @author Mr.Fang
 * @description 在职状态以及工作性质实体类
 * @date 2021/8/16
 */

@Data
public class WorkStatus {
    //在职状态
    //在职
    private int onthejob;
    //已离职
    private int resign;
    //待入职
    private int tobehired;
    //实习期
    private int practice;
    //试用期
    private int tryout;
    //正式
    private int formal;

    //........................................//
    //工作性质
    //全职
    private int fulltime;
    //兼职
    private int parttime;
    //劳务派遣
    private int laborDispatch;
    //外包
    private int outsourcing;
    //退休返聘
    private int retirement;

}
