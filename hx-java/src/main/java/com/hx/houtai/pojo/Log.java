package com.hx.houtai.pojo;

import lombok.Data;

/**
 * @author Mr.Fang
 * @description 日志实体类
 * @date 2021/7/30
 */

@Data
public class Log {

    //主键
    private String logGuid;

    //操作用户
    private String logUser;

    //ip
    private String logClientIP;

    //请求URL
    private String logUrl;

    //请求方法
    private String logMethod;

    //请求参数
    private String logArgs;

    //返回参数
    private String logResult;

    //日志类型
    private String logType;

    //创建时间
    private String logTime;
}
