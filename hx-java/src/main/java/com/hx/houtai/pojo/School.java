package com.hx.houtai.pojo;


import lombok.Data;

/**
 * @author Mr.Fang
 * @description 院校实体类
 * @date 2021/3/3
 */

@Data
public class School {

    private int baseId;
    private String baseSchoolName;
}
