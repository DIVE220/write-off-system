import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/employeeInfo'
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/Login')
  },
  {
    path: '/newPage',
    name: 'NewPage',
    component: () => import('../views/NewPage'),
    children: [
      {
        path: '/AddEmployee',
        name: 'AddEmployee',
        // component: () => import('../views/NewPageGroup/AddEmployee')
        components: {
          newPage: () => import('../views/NewPageGroup/AddEmployee')
        }
      },
      {
        path: '/AddTobehired',
        name: 'AddEmployee',
        components: {
          newPage: () => import('../views/NewPageGroup/AddTobehired')
        }
      },
    ]
  },

]

import adde from '../views/NewPageGroup/AddEmployee'

export const newPageRouterMap = {}

import employeeInfo from '../views/BasicInfo.vue'

export const asyncRouterMap = {

  'employeeInfo': {
    path: '/employeeInfo',
    name: '员工资料',
    components: {
      home: () => import('../views/BasicInfo.vue')
    }
    // component: () =>
  },
  'departmentInfo': {
    path: '/departmentInfo',
    name: '部门资料',
    component: () => import('../views/AdvancedInfo.vue')
  },
  'productionSalary': {
    path: '/productionSalary',
    name: '工资制作',
    component: () => import('../views/ProductionSalary.vue')
  },
  'historySalary': {
    path: '/historySalary',
    name: '工资历史',
    component: () => import('../views/HistorySalary.vue')
  },
  'staffRewardAndPunish': {
    path: '/staffRewardAndPunish',
    name: '员工奖惩',
    component: () => import('../views/StaffRewardAndPunish.vue')
  },
  'staffTrain': {
    path: '/staffTrain',
    name: '员工培训',
    component: () => import('../views/StaffTrain.vue')
  },
  'staffSalaryAdj': {
    path: '/staffSalaryAdj',
    name: '员工调薪',
    component: () => import('../views/StaffSalaryAdj.vue')
  },
  'staffMobilize': {
    path: '/staffMobilize',
    name: '员工调动',
    component: () => import('../views/StaffMobilize.vue')
  },
  'attendanceManage': {
    path: '/attendanceManage',
    name: '考勤管理',
    component: () => import('../views/AttendanceManage.vue')
  },
  'basisInfoSetup': {
    path: '/basisInfoSetup',
    name: '基础信息设置',
    component: () => import('../views/BasisInfoSetup.vue')
  },
  'logManage': {
    path: '/logManage',
    name: '日志管理',
    components: {
      home: () => import('../views/LogManage.vue')
    }
  },
}

const router = new VueRouter({
  routes
})

//重置路由
export function resetRouter() {
  const newRouter = router;
  router.matcher = newRouter.matcher;
}

// 解决双击路由报错
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(to) {
  return originalPush.call(this, to).catch(err => err)
}
export default router

