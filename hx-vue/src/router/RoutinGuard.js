// 路由守卫（引入router菜单的路由）
import router, { asyncRouterMap } from './index';
import store from '../store/index';
import {Message} from 'element-ui'
// state 映射 路由方法；
import {
  menusToRoutes
} from '../store/mapnav'
import de from "element-ui/src/locale/lang/de";
// 定义锁
let hasMenus = false;
// 路由守卫
router.beforeEach((to, from, next) => {
  // 常用token 登入
  // 如果有token 直接跳
  if (sessionStorage.getItem('token')) {
    // 如果有token 直接跳；
    if (to.path === '/login') {
      next({
        path: '/'
      })
    } else {
      if (hasMenus) {
      } else {
        // 重新映射新路由
        try {
          // 重新渲染新路由，
          const routes = menusToRoutes(store.state.menuItems);
          // const routess = menusToRoutes(store.state.menuItemsTwo)
          router.addRoutes(routes);
          // router.addRoutes(routess);
          hasMenus = true;
          // 放行
          next({
            path: to.path
          })
        } catch (error) {
          next('/login')
        }
      }
    }
  } else {
    if(hasMenus){
      Message.error('未授权，请重新登录');
    }
    hasMenus = false;
    //如果即将要进入到login页面 并且没有token 那证明用户是第一次登入。这种情况直接放行。
    if (to.path === '/login') {
      next();
      return;
    } else {
      next('/login');
      // 如果即将要进到 login 页面 并且没有token 的话， 返回到login 页面
      return;
    }
  }
  next();
})
