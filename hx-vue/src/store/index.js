import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
export default  new Vuex.Store({
  state: {
    menuItems:[
      {
        // 要跳转的路由名称 不是路径
        text: "员工管理", // 文本内容
        img: "基础信息",
        children: [
          {
            name: "employeeInfo",
            text: "员工花名册",
          },
          {
            name: "departmentInfo",
            text: "部门资料"
          }
        ]
      },
      {
        text: "工资管理",
        img: "工资管理",
        children: [
          {
            name: "productionSalary",
            text: "工资制作"
          },
          {
            name: "historySalary",
            text: "工资历史"
          }
        ]
      },
      {
        text: "人事管理",
        img: "人事管理",
        children: [
          {
            name: "staffRewardAndPunish",
            text: "员工奖惩",
          },
          {
            name: "staffTrain",
            text: "员工培训",
          },
          {
            name: "staffSalaryAdj",
            text: "员工调薪",
          },
          {
            name: "staffMobilize",
            text: "员工调动",
          },
        ]
      },
      {
        text: "系统管理",
        img: "系统设置",
        children: [
          {
            name: "basisInfoSetup",
            text: "基础信息设置"
          },
          {
            name: "logManage",
            text: "操作日志管理"
          }
        ]
      },
      {
        name: "attendanceManage",
        text: "考勤管理",
        img: "考勤管理",
      },

    ],
    menuItemsTwo:[
      {
        name: "AddEmployee",
        text: "添加员工",
      },
      {
        name: "AddTobehired",
        text: "添加待入职",
      }
    ],
    token: "",
  },
  getters: {
    keepAliveTag(state){
      return state.tags.map(item => item.data);
    }
  }
})
