import {asyncRouterMap} from "../router/index";

export function menusToRoutes(data){
  const result = [];
  const children = [];
  result.push({
    path: '/',
    component: () => import('../components/index'),
    children,
  })

  data.forEach(item => {
    generateRoutes(children , item)
  })
  return result

}

export function menusToRoutesTwo(data){
  const result = [];
  const children = [];
  result.push({
    path: '/',
    component: () => import('../views/NewPage'),
    children,
  })

  data.forEach(item => {
    generateRoutes(children , item)
  })
  return result

}

function generateRoutes(children , item){
  if(item.name){
    children.push(asyncRouterMap[item.name]);
  }else if(item.children){
    item.children.forEach(e => {
      generateRoutes(children, e)
    })
  }
}
