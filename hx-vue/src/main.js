// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import 'element-ui/lib/theme-chalk/base.css'
import CollapseTransition from 'element-ui/lib/transitions/collapse-transition'
import api from '../src/api/api';     // 接口模块
import axios from 'axios';
import './icons'
import store from '../src/store/index'
import './router/RoutinGuard'
import echarts from 'echarts'

Vue.prototype.$axios = axios;   //全局注册，使用方法为:this.$axios
Vue.config.productionTip = false
Vue.prototype.$api = api;
Vue.prototype.$store = store;
Vue.prototype.$echarts = echarts;
Vue.use(ElementUI)
Vue.component(CollapseTransition.name,CollapseTransition)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
}).$mount('#app')
