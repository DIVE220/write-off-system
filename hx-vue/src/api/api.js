import {post} from '../axios/config'
import fa from "element-ui/src/locale/lang/fa";


export default {
  //登录
  Login(params) {
    return post('/login', params)
  },
  //查询log日志
  selLogInfo(params){
    return post('/FL/selLogInfo' , params)
  },
  //查询员工页面的部门信息
  selEmDepartmentInfo(params){
    return post('/FL/selEmDepartmentInfo' , params)
  },
  //查询员工页面员工信息
  selEmployeeInfo(params){
    return post('/FL/selEmployeeInfo' , params)
  },
  //查询不同状态的人数
  selworkStatusNum(params){
    return post('/FL/selworkStatusNum' , params)
  },
  //获取部门职位树
  seldePostTree(params){
    return post('/FL/seldePostTree' , params)
  },
  //修改员工简单信息
  updateEmployeeInfo(params){
    return post('/FL/updateEmployeeInfo' , params)
  },
  //查询院校
  selSchool(params){
    return post('/FL/selSchool' , params)
  },
  //新增员工
  addEmployee(params){
    return post('/FL/addEmployee' , params)
  },
}
