import {Loading} from 'element-ui'
import pageLoading from '../assets/css/pageLoading.css'

let loadingCount = 0;
let loading;

const startLading = () => {
  loading = Loading.service({
    lock: true,
    spinner: 'iconfont iconjiazai animal',
    background: 'rgba(0,0,0,0.7)'
  })
};

const endLoading = () => {
  loading.close();
};

export const showLoading = () => {
  if (loadingCount === 0) {
    startLading();
  }
  loadingCount += 1;
};
export const hideLoading = () => {
  if (loadingCount <= 0) {
    return;
  }
  loadingCount -= 1;
  if (loadingCount === 0) {
    endLoading();
  }
};
